import getFrontendWeight from "@/utils/get_frontend_weight";
import fillerLabels from "../../utils/filler_labels";

describe("getFrontendWeight", () => {
  describe("with correct labels", () => {
    it.each`
      label                    | weight
      ${"frontend-weight::1"}  | ${0.5}
      ${"frontend-weight::2"}  | ${1}
      ${"frontend-weight::3"}  | ${3}
      ${"frontend-weight::5"}  | ${5}
      ${"frontend-weight::8"}  | ${13}
      ${"frontend-weight::13"} | ${20}
    `("should parse the $label as $weight", ({ label, weight }) => {
      const result = getFrontendWeight([...fillerLabels, { title: label }]);

      expect(result).toEqual(weight);
    });
  });

  describe("with invalid labels", () => {
    it("should render 0 when there are no frontend weight labels", () => {
      const result = getFrontendWeight(fillerLabels);

      expect(result).toEqual(0);
    });

    it("should render the number with an invalid frontend weight number", () => {
      const result = getFrontendWeight([
        ...fillerLabels,
        { title: "frontend-weight::99" },
      ]);

      expect(result).toEqual(99);
    });

    it("should render 0 with a completely invalid frontend weight label", () => {
      const result = getFrontendWeight([
        ...fillerLabels,
        { title: "frontend-weight::ten" },
      ]);

      expect(result).toEqual(0);
    });
  });
});
