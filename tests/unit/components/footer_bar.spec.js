import { shallowMount } from "@vue/test-utils";
import FooterBar from "@/components/footer_bar.vue";
import LoginWithGitlab from "@/components/login_with_gitlab.vue";
import CurrentUser from "@/components/current_user.vue";

let wrapper;
const user = {
  avatarUrl: "http://placecage.com/300",
  username: "nick_cage",
};

const createComponent = (props) => {
  wrapper = shallowMount(FooterBar, {
    propsData: props,
  });
};

describe("Footer bar", () => {
  afterEach(() => {
    wrapper.destroy();
  });

  describe("With no user", () => {
    beforeEach(() => {
      createComponent();
    });

    it("should render the login button", () => {
      expect(wrapper.find(LoginWithGitlab).exists()).toBe(true);
    });

    it("should not render the current user", () => {
      expect(wrapper.find(CurrentUser).exists()).toBe(false);
    });
  });

  describe("With a user", () => {
    beforeEach(() => {
      createComponent({ user });
    });

    it("should render the current user", () => {
      expect(wrapper.find(CurrentUser).exists()).toBe(true);
    });
  });
});
