export default (date) => {
  // Note: This only works for 14.0 and beyond as we
  // shifted our release schedule in April 2020
  // At least, until we shift it again

  const year = parseInt(date.getFullYear(), 10);
  const month = parseInt(date.getMonth(), 10);
  const day = parseInt(date.getDate(), 10);

  const lastDay = 14;
  let firstMonth = 5;
  let major = year - 2007;
  let minor = month - firstMonth;

  if (day > lastDay) {
    minor += 1;
    firstMonth -= 1;
  }

  if (month < firstMonth) {
    major -= 1;
  }

  return `${major}.${(minor + 12) % 12}`;
};
