export default (milestone) => {
  // Note: This only works for 13.0 and beyond as we
  // shifted our release schedule in April 2020
  // At least, until we shift it again

  // Note: Months start at 0. e.g.
  // Jan = 0
  // April = 3
  // December = 11

  const [major, minor] = milestone.split(".").map((m) => parseInt(m, 10));
  const date = 18;
  const firstMonth = 4;
  let month = (minor + firstMonth) % 12;
  let year = major + 2007;

  if (month < firstMonth) {
    year += 1;
  }

  // Because 12.x is off by one month
  if (major === 12 && minor < 11) {
    month += 1;
  }

  return new Date(year, month, date);
};
